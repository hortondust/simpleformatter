from django.conf.urls import patterns, include, url
from simpleformatter.views import MainView, submit_text

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'simpleformatter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
	(r'^format-text/$', submit_text),
	(r'^$', MainView.as_view()),
	
)
