
import formatters
import logging

from django.views.generic import TemplateView

from django.shortcuts import render
from simpleformatter.forms import FormatterForm


class MainView(TemplateView):
    template_name = "main_page.html"


class ResultView(TemplateView):
    template_name = "result_page.html"


def submit_text(request):
    if request.method == 'POST':
        form = FormatterForm(request.POST)
        # check whether it's valid: TODO
        if form.is_valid():
            text_format = form.cleaned_data['textformat']
            text = form.cleaned_data['text']
            indentation = int(form.cleaned_data['indentation'])
            if text_format == 'json':
                formatted = formatters.format_json(text, indentation)
            elif text_format == 'xml':
                formatted = formatters.format_xml(text, indentation)
            else: #html
                formatted = formatters.format_html(text, indentation)
        else:
            # TODO: handle invalid form
            logging.warn('form is not valid')

        return render(request, 'result_page.html', {'text': formatted})

