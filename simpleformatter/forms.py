from django import forms


class FormatterForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea)
    FORMAT_CHOICES = (('json', 'json',), ('xml', 'xml',), ('html', 'html',))
    textformat = forms.ChoiceField(choices=FORMAT_CHOICES,widget=forms.RadioSelect())
    INDENTATION_CHOICES = (('2','2 spaces',), ('3','3 spaces'), ('4','4 spaces'), ('0','Remove Formatting'))
    indentation = forms.ChoiceField(choices=INDENTATION_CHOICES)