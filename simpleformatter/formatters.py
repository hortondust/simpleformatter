
import xml.dom.minidom
import json
import collections
import logging

from lxml import etree
from bs4  import BeautifulSoup

def format_json(json_string, spaces):
    # making everything UTF 8 right now
    logging.info(spaces)
    parsed_json = json.loads(json_string, object_pairs_hook=collections.OrderedDict)
    if spaces == 0:
        formatted = json.dumps(parsed_json, separators=(',', ':'), sort_keys=False, ensure_ascii=False).encode('utf8')
    else:
        formatted = json.dumps(parsed_json, indent=spaces, sort_keys=False, ensure_ascii=False).encode('utf8')
    return formatted



def format_xml( xml_string, spaces):
    # everything UTF 8 right now
    if spaces == 0:
        parser = etree.XMLParser(remove_blank_text=True)
        etreexml = etree.XML(xml_string.encode('utf-8'), parser=parser)
        formatted = etree.tostring(etreexml)
    else:
        parsed_xml = xml.dom.minidom.parseString(xml_string.encode('utf-8'))
        formatted = parsed_xml.toprettyxml(indent=' ' * spaces, encoding='utf-8')
        #the next 4 lines are a hack way to remove the xml declaration
        doc = xml.dom.minidom.Document()
        declaration = doc.toxml('utf-8')
        if declaration.lower() not in xml_string.lower():
            formatted = formatted[len(declaration):]
    return formatted


def format_html(html_string, spaces):
    #not supporting the spaces parameter just yet
    return BeautifulSoup(html_string, 'lxml').prettify( encoding='utf-8', formatter='minimal')